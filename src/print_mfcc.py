#coding:utf-8
import struct
import sys
import numpy as np

def load_mfcc(mfccFile, m):
	# floatの数値データを格納する配列
	mfcc = []
	# mode read binary
	f = open(mfccFile, "rb")
	while True:
		# 4バイトずつ読み込む
		b = f.read(4)
		if b == "": break;
		# バイナリを float型に変換する
		val = struct.unpack("f", b)[0]
		mfcc.append(val)
	f.close()
	mfcc = np.array(mfcc)
	numframe = len(mfcc) / m
	if numframe * m != len(mfcc):
		print "printMFCC ERROR: #mfcc:%d #frame:%d m:%d" % (len(mfcc), numframe, m)
		sys.exit(1)
	# 配列の形式を m個の配列を numframe個作成するように変更する
	mfcc = mfcc.reshape(numframe, m)
	return mfcc
	
if __name__ == "__main__":
	# バイナリデータである、mfcファイルを数値で表示させる
	# 引数の数を制限する
	if len(sys.argv) != 3:
		print "usage: python print_mfcc.py [mfcc_file] [m]"
		print "m = mfcc dimension + 1 ( which is power spectrum )"
		sys.exit(1)
	mfccFile = sys.argv[1]
	# m は mfccの次元数 + 1（パワースペクトル）で、x2xコマンドで、-mオプションに指定する数 + 1
	m = int(sys.argv[2])
	mfcc = load_mfcc(mfccFile, m)
	
	for i in range(len(mfcc)):
		print "\t".join("%.2f" % x for x in mfcc[i,])
	