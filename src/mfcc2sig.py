#coding:utf-8
import os
import sys
import numpy as np
import scipy.cluster.vq
import print_mfcc

def vectorQuantization(mfcc, k):
	"""mfccのベクトル集合をk個のクラスタにベクトル量子化"""
	# codebookはk近傍法で求めた、各クラスタの中心を指す特徴ベクトル
	# distortion は中心との差を3乗した値の和（k-meansではこれをある程度まで小さくする）
	codebook, distortion = scipy.cluster.vq.kmeans(mfcc, k)
	# ベクトル量子化で、mfccの行列を少数の特徴ベクトルに変換
	code, dist = scipy.cluster.vq.vq(mfcc, codebook)
	return code

def mfccToSigniture(mfccFilePath, sigFilePath):
	# mfccファイルをsignitureファイルに変換する（要はベクトル量子化して、ベクトルの次元を削除する）
	fout = open(sigFilePath, "w")
	# MFCCをロード
	# 各行がフレームのMFCCベクトル
	mfcc = print_mfcc.load_mfcc(mfccFilePath, 20)
	# MFCCをベクトル量子化して特徴を求める
	code = vectorQuantization(mfcc, 16)

	# 各クラスタのデータ数、平均ベクトル、共分散行列を求めてシグネチャとする
	for k in range(16):
		# クラスタkのフレームのみ抽出
		frames = np.array([mfcc[i] for i in range(len(mfcc)) if code[i] == k])
		# MFCCの各次元の平均をとって平均ベクトルを求める
		m = np.apply_along_axis(np.mean, 0, frames)  # 0は縦方向
		# MFCCの各次元間での分散・共分散行列を求める
		sigma = np.cov(frames.T)
		# 重み（各クラスタのデータ数）
		w = len(frames)
		# このクラスタの特徴量をフラット形式で出力
		# 1行が重み1個、平均ベクトル20個、分散・共分散行列400個の計421個の数値列
		features = np.hstack((w, m, sigma.flatten()))
		features = [str(x) for x in features]
		fout.write(" ".join(features) + "\n")
	fout.close()

def mfccFileListToSigFileList(mfccFilePathList):
	sigFilePathList = []
	print "mfcc -> sig : total : ", len(mfccFilePathList), " : ",
	for i, mfccFilePath in enumerate(mfccFilePathList):
		sigFilePath = mfccFilePath.replace(".mfc", ".sig")
		if not os.path.exists(mfccFilePath) or os.path.exists(sigFilePath) : continue
		# mfcc -> sig
		mfccToSigniture(mfccFilePath, sigFilePath)
		print i, " ",
	return sigFilePathList
	

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python mfcc2sig.py [mfccdir] [sigdir]"
		sys.exit(1)
	mfccDir = sys.argv[1]
	sigDir  = sys.argv[2]
	if not os.path.exists(sigDir): os.mkdir(sigDir)
	for mfccFile in os.listdir(mfccDir):
		if not mfccFile.endswith(".mfc"): continue
		mfccFilePath = os.path.join(mfccDir, mfccFile)
		sigFilePath = os.path.join(sigDir, mfccFile.replace(".mfc", ".sig"))
		mfccToSigniture(mfccFilePath, sigFilePath)

