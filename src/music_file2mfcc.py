#coding:utf-8
import os
import sys
import file_utilities as fu
		

def m4aToWav(m4aFilePath, wavFilePath):
	# m4aからwavへの変換
	os.system("afconvert -d LEI24 -f WAVE '%s' '%s'" % (m4aFilePath, wavFilePath))

def mp3ToWav(mp3FilePath, wavFilePath):
	# mp3からwavへの変換
	os.system("afconvert -d MPG3 -f WAVE '%s' '%s'" % (mp3FilePath, wavFilePath))

def wavToRaw(wavFilePath, rawFilePath):
	# wavからrawへの変換
	os.system("sox -t wav '%s' '%s'" % (wavFilePath, rawFilePath))
		
def musicFileToRaw(musicFilePath, rawFilePath):
	# 音楽ファイルをrawに変換
	headTailTuple = os.path.split(musicFilePath)
	if headTailTuple[0] != None and headTailTuple[1] != None:
		wavFileName = "temp.wav"
		# 一時的なwavファイルを作成
		tempWavFilePath = os.path.join(headTailTuple[0], wavFileName)
		if musicFilePath.endswith(".m4a"):
			m4aToWav(musicFilePath, tempWavFilePath)
		elif musicFilePath.endswith(".mp3"):
			mp3ToWav(musicFilePath, tempWavFilePath )
		else:
			return
		wavToRaw(tempWavFilePath, rawFilePath)
		# wavファイルを削除
		os.remove(tempWavFilePath)
		
def calcNumSample(rawAbsPath):
	# サンプル数（音階データの数）の取得
	filesize = os.path.getsize(rawAbsPath)
	# 1サンプルはshort型（2byte）なので（rawfileだから）ファイルサイズを2で割る
	numsample = filesize / 2
	return numsample

def trimCenter(inFilePath, outFilePath, periodInSec = 30, fs = 16000):
	# 楽曲の全てを取得するのはデータが多すぎるので、曲の中心からトリムする
	# 波形のサンプル数を求める
	numsample = calcNumSample(inFilePath)
	if numsample < fs * periodInSec: return False
	center = numsample / 2
	start = center - fs * periodInSec/2
	end = center + fs * periodInSec/2
	# periodInSec*2 秒未満の場合は範囲を狭める
	if start < 0: start = 0
	if end > numsample - 1: end = numsample - 1
	# SPTKのbcutコマンドで切り出す
	os.system("bcut +s -s %d -e %d < '%s' > '%s'" % (start, end, inFilePath, outFilePath))
	return True

def calcMFCC(rawAbsPath, mfccAbsPath):
	# サンプリング周波数: 16kHz
	# フレーム長: 400サンプル
	# シフト幅: 160サンプル
	# チャンネル数: 40
	# MFCC: 19次元 + エネルギー
	# 30秒 * 16000Hz /160 = 3000 フレーム 
	# x2x +sf は short -> float変換 
	os.system("x2x +sf < '%s' | frame -l 400 -p 160 | mfcc -l 400 -f 16 -n 40 -m 19 -E > '%s'" % (rawAbsPath, mfccAbsPath))
	
def musicFileToMfcc(musicFilePath, mfccFilePath = None, mfccDirPath = None):
	# 音楽ファイルからmfccファイルを創り出す
	if mfccFilePath is None and mfccDirPath is not None:
		if not os.path.exists(mfccDirPath): return
		mainDir = os.path.dirname(mfccDirPath)
		mfccFilePath = musicFilePath.replace(mainDir, mfccDirPath)
		mfccFilePath = fu.changeExtension(mfccFilePath, [".m4a",".mp3"], ".mfc")
		fu.makeDirsForPath(mfccFilePath)
	if mfccFilePath is not None:
		try:
			rawAbsPath = os.path.join(os.path.dirname(musicFilePath), "temp.raw")
			trimmedRawAbsPath = os.path.join(os.path.dirname(musicFilePath), "trimmed_temp.raw")
			musicFileToRaw(musicFilePath, rawAbsPath)
			# 中央の30秒だけ抽出してrawFileへ
			trimCenter(rawAbsPath, trimmedRawAbsPath)
			# MFCCを計算
			calcMFCC(trimmedRawAbsPath, mfccFilePath)
		except:
			return
		finally:
			if os.path.exists(rawAbsPath): os.remove(rawAbsPath)
			if os.path.exists(trimmedRawAbsPath): os.remove(trimmedRawAbsPath)

def musicFileListToMfccList(musicFilePathList, mfccFilePathList = None, mainDir = None):
	# 音楽ファイルの入ったリストをすべてmfccに変換する
	if mainDir is None and mfccFilePathList is None: 
		print "at musicFileListToMfccList : ERROR : wrong args ..."
		return
	# mfccのリストがある場合
	if mainDir is None:
		if len(musicFilePathList) is not len(mfccFilePathList): return
		print "musicfile -> mfcc : total ", len(musicFilePathList)," : ", 
		for i in range(len(musicFilePathList)):
			musicFilePath = musicFilePathList[i]
			mfccFilePath = mfccFilePathList[i]
			musicFileToMfcc(musicFilePath, mfccFilePath)
			print i, " ", 
				
	# mfccを格納する親ディレクトリを指定された場合
	else:
		# mfccディレクトリを作成する
		mfccDirPath = os.path.join(mainDir, "mfcc")
		if not os.path.exists(mfccDirPath): os.mkdir(mfccDirPath)
		print "musicfile -> mfcc : total ", len(musicFilePathList)," : ",
		for i, musicFilePath in enumerate(musicFilePathList):
			musicFileToMfcc(musicFilePath, mfccFilePath=None, mfccDirPath=mfccDirPath)
			print i, " ",
			
if __name__ == "__main__":
	# m4aファイルからMFCCファイルに変換する
	# スクリプトとして動かす
	if len(sys.argv) != 2:
		print "usage: python music_file2mfcc.py [m4a(or mp3)dir]"
		sys.exit(1)
	mainDir = sys.argv[1]
	musicFilePathList = fu.getAllFiles(mainDir, extensions=[".m4a", ".mp3"])
	musicFileListToMfccList(musicFilePathList, mainDir=mainDir)
	


