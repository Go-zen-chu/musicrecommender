#coding:utf-8
import sys
import os
import music_file2mfcc
import mfcc2sig

def setupDirectories(parentPath):
	if not os.path.exists(parentPath):
		print "Couldn't find path '%s'" % parentPath
		sys.exit(1)
	myPath = os.path.join(parentPath, "musicRecommender/")
	if not os.path.exists(myPath): os.mkdir(myPath)
	
	musicFilePathList = []
	mfccFilePathList = []
	# parentPathを再帰的に調査し、楽曲ファイルを変換したものがなければリストに追加
	for root, dirs, files in os.walk(parentPath):
		for fileName in files:
			if not fileName.endswith(".m4a") and not fileName.endswith(".mp3"): continue		
			extension = os.path.splitext(fileName)[1]
			# 変換前のパス
			musicFilePath = os.path.join(root, fileName)
			# 返還後のパス
			mfccFilePath = musicFilePath.replace(parentPath, myPath).replace(extension, ".mfc")
			# まだ変換されていない場合
			if not os.path.exists(mfccFilePath):
				musicFilePathList.append(os.path.join(root, fileName))
				mfccFilePathList.append(mfccFilePath)
				dirPath = os.path.dirname(mfccFilePath)
				if not os.path.exists(dirPath):
					os.makedirs(dirPath) 
	return musicFilePathList, mfccFilePathList

if __name__ == '__main__':
	# Set up （m4aの大量に入ったディレクトリの構成と全く同じ構成のsigファイル集を作成）
	if len(sys.argv) != 2:
		print "usage: python music_recommender.py [musics parent directory]"
		sys.exit(1)
	# musicPathから再帰的にすべての音楽ファイルを取得する
	musicPath = sys.argv[1]
	musicFilePathList, mfccFilePathList =  setupDirectories(musicPath)
	print len(musicFilePathList), " files will be processed"
	# mfccフォルダを作成し、そこに保存するようにする
	music_file2mfcc.musicFileListToMfccList(musicFilePathList, mfccFilePathList)
	# mfcc -> sig
	sigFilePathList = mfcc2sig.mfccFileListToSigFileList(mfccFilePathList)
	for sigFilePath in sigFilePathList:
		
	
	
	