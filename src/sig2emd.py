#coding:utf-8
import emd
import numpy as np
import numpy.linalg
import sys

def loadSignature(sigFilePath):
	"""シグネチャファイルをロード"""
	matrix = []
	fp = open(sigFilePath, "r")
	for line in fp:
		line = line.rstrip()
		matrix.append([float(x) for x in line.split()])
	fp.close()
	return np.array(matrix)

def KLDivergence(mu1, S1, mu2, S2):
	"""正規分布間のカルバックライブラー情報量（ただし、距離の比較に用いるため、定数項や行列式の項は削除した）"""
	# カルバック・ライブラー情報量は、情報利得の平均である
	# 大きいほど、情報利得が大きい->良いデータを得たということ
	# 逆行列を計算
	try:
		invS2 = np.linalg.inv(S2)
	except numpy.linalg.linalg.LinAlgError:
		raise;
	# KL Divergenceを計算
	# tr()はトレースで、対角成分の和 diagで対角成分を取得し、sumする
	t1 = np.sum(np.diag(np.dot(invS2, S1)))
	t2 = (mu2 - mu1).transpose()
	t3 = mu2 - mu1
	distance = t1 + np.dot(np.dot(t2, invS2), t3)
	# dotは行列やベクトルの積
	return distance[0,0]

def symmetricKLDivergence(mu1, S1, mu2, S2):
	"""対称性のあるカルバック・ライブラー情報量"""
	# m は平均（ベクトル）、sは共分散（行列）
	# カルバック・ライブラー情報量には、対称性がなく、P=>Q とQ=>Pで取る値が変わる
	# そのため、それらを折半したものをKLDivergenceとする
	return 0.5 * (KLDivergence(mu1, S1, mu2, S2) + KLDivergence(mu2, S2, mu1, S1))


def distanceMethod(feature1, feature2):
	'''特徴ベクトル間の距離を求める（featuresではないことに注意）'''
	mu1 = np.array(feature1[0:20]).reshape(20, 1)
	mu2 = np.array(feature2[0:20]).reshape(20, 1)
	S1 = np.array(feature1[20:421]).reshape(20, 20)
	S2 = np.array(feature2[20:421]).reshape(20, 20)
	# KLダイバージェンスを計算
	return symmetricKLDivergence(mu1, S1, mu2, S2)
	

def calcEmd(sigFilePath1, sigFilePath2):
	sigData1 = loadSignature(sigFilePath1)
	sigData2 = loadSignature(sigFilePath2)
	# シグネチャの重み（0列目）を取得（16,1）
	weights1 = sigData1[:,0].tolist()
	weights2 = sigData2[:,0].tolist()
	# それ以外の箇所（1-421列）を特徴ベクトルの集合とする(16,420)
	features1 = sigData1[:,1:].tolist()
	features2 = sigData2[:,1:].tolist()
	# ライブラリのemdメソッド．weightは (n,1)ベクトル、featureは(n,m)ベクトルでなかればならない
	# また、pythonの標準のlist形式でなかれば、まともに動かない
	return emd.emd((features1, weights1), (features2, weights2), distanceMethod)
	
if __name__ == "__main__":
	'''EMDとKL情報量を用いて、シグネチャの間の距離を求める'''
	if len(sys.argv) != 3:
		print "usage: python sig2emd.py [sigFilePath1] [sigFilePath2]"
		sys.exit(1)
	print calcEmd(sys.argv[1], sys.argv[2])
