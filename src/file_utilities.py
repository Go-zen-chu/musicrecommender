#coding:utf-8
import os

def makeDirsForPath(filePath):
	# あるパスの親ディレクトリをすべて作成
	dirPath = os.path.dirname(filePath)
	if not os.path.exists(dirPath):
		os.makedirs(dirPath)
		
def getAllFiles(dirPath, extensions = None):
	# あるディレクトリ以下の全てのファイルを取得
	# extensionsを指定すると、その拡張子のファイルのみを取得
	if not os.path.exists(dirPath): return
	fileList = []
	for root, dirs, files in os.walk(dirPath):
		for fileName in files:
			if extensions is not None:
				if hasExtension(fileName, extensions):
					fileList.append(os.path.join(root, fileName))
			else:
				fileList.append(os.path.join(root, fileName))
	return fileList

def hasExtension(path, extensions):
	# sample:  path = "file.py"  extensions = [".py", ".pyc"]
	if path is None or extensions is None: return
	ext = os.path.splitext(path)[1]
	if ext in extensions:
		return True
	else:
		return False

def changeExtension(path, extensions, newExtension):
	extTuple = os.path.splitext(path)
	if extTuple[1] in extensions:
		return extTuple[0] + newExtension
	else:
		return path
	
	
	
	
	
	