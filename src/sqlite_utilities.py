#coding:utf-8
import sqlite3	
	
def is_table_exists(self, table_name):
	result = select(u"sqlite_master", u"count(*)", (u"type='table' AND name=%s", table_name)) 
	return False if result is 0 else True

def create_table(self, table_name, column_definition_phrase):
	if is_table_exists(table_name) is False:
		self.cursor.execute(u"CREATE TABLE %s (%s)", table_name, column_definition_phrase)
	else:
		print "Table ", table_name, " is already exists!"

def select(self, from_phrase, what_phrase, where_phrase):
	return self.cursor.execute(u"SELECT %s FROM %s WHERE %s", (what_phrase, from_phrase, where_phrase))

def insert(self, table_name, values):
	self.cursor.execute("INSERT INTO %s VALUES %s", table_name, values)

def is_value_exists(self, table_name, from_phrase, where_phrase):
	result = select(table_name, from_phrase, where_phrase)
	return False if len(result) is 0 else True
	
class SqliteUtilities:
	
	def __init__(self, database_name):
		if database_name is None: return
		self.database_name = database_name
		self.connection = sqlite3.connect(database_name)
		self.cursor = self.connection.cursor()
	
	def __del__(self):
		if self.cursor is not None: self.cursor.close()
		if self.connection is not None: self.connection.close()
	
	